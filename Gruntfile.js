// https://scotch.io/tutorials/a-simple-guide-to-getting-started-with-grunt

// $ grunt watch

// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt) {

  // ===========================================================================
  // CONFIGURE GRUNT ===========================================================
  // ===========================================================================
  grunt.initConfig({

    // get the configuration info from package.json ----------------------------
    // this way we can use things like name and version (pkg.name)
    pkg: grunt.file.readJSON('package.json'),

    // all of our configuration will go here

    // configure jshint to validate js files -----------------------------------
    jshint: {
      options: {
        reporter: require('jshint-stylish') // use jshint-stylish to make our errors look and read good
      },

      // when this task is run, lint the Gruntfile and all js files in src
      build: ['Gruntfile.js', 'giga-ng/js/*.js']
    },

    concat: {
      // build: {
        build: { //target
            src: ['giga-ng/js/app.module.js', 'giga-ng/js/app.factory.js', 'giga-ng/js/app.routes.js', 'giga-ng/js/app.animations.js', 'giga-ng/js/app.run.js', 'giga-ng/js/shared/**/*.js', 'giga-ng/js/components/**/*.js'],
            dest: 'giga-ng/js/functions.js'
        }
      // }
    },

    uglify: {
      options: {
        banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
      },
      build: {
        files: {
          'destination-theme/js/functions.min.js': 'giga-ng/js/functions.js'
        }
      }
    },

    sass: {
      build: {
        files: {
          'giga-ng/style.css': 'giga-ng/style.scss'
        }
      }
    },

    cssmin: {
      options: {
        banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
      },
      build: {
        files: {
          'destination-theme/style.min.css': 'giga-ng/style.css'
        }
      }
    },

    sync: {
      main: {
        files: [{
          cwd: 'giga-ng',
          src: [
            '**',
            // '!**/functions.js',  WP Production
            '!*.scss',
            // '!*.css',            WP Production
            '!*.map',
            '!**/*.scss',
            // '!**/*.css',         WP Production
            '!**/*.map',
          ],
          dest: 'destination-theme',
        }],
        pretend: false,
        verbose: true
      }
    },

    watch: {

      stylesheetsSass: {
        files: 'giga-ng/**/*.scss', tasks: ['sass']
      },

      // WP need style.css
      // stylesheetsMin: {
      //   files: 'giga-ng/**/*.css', tasks: ['cssmin']
      // },

      // WP Production
      scripts: {
        files: 'giga-ng/**/*.js', tasks: ['jshint', 'concat', 'uglify']
      },

      // WP Test
      // scripts: {
      //   files: 'giga-ng/**/*.js', tasks: ['jshint']
      // },

      otherFiles: {
        files: 'giga-ng/**', tasks: ['sync']
      }


    },

    // ============= // CREATE TASKS ========== //

  });

  // ===========================================================================
  // LOAD GRUNT PLUGINS ========================================================
  // ===========================================================================
  // we can only load these if they are in our package.json
  // make sure you have run npm install so our app can find these
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'sass', 'cssmin', 'sync']);


};
