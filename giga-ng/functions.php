<?php

// Setup
require get_template_directory() . '/inc/setup.php';

// Images Setup
require get_template_directory() . '/inc/setup-images.php';

// Admin Setup
require get_template_directory() . '/inc/setup-admin.php';

// Frontend Setup
require get_template_directory() . '/inc/setup-frontend.php';

// Rest API
require get_template_directory() . '/inc/rest-api.php';

// Theme Settings

/*
* Url Rewritting
*/

add_action( 'init', 'custom_page_rules' );

function custom_page_rules() {
  global $wp_rewrite;
  $wp_rewrite->page_structure = $wp_rewrite->root . 'pages/%pagename%';
}

/**
 * Add new rewrite rule
 */

function create_new_url_querystring() {
    add_rewrite_rule(
        'posts/([^/]*)$',
        'index.php?name=$matches[1]',
        'top'
    );
    add_rewrite_tag('%post%','([^/]*)');
}
add_action('init', 'create_new_url_querystring', 999 );

/*
 * Modify post link
 * This will print /post/post-name instead of /post-name
 */

function append_query_string( $url, $post, $leavename ) {
	if ( $post->post_type == 'post' ) {
		$url = home_url( user_trailingslashit( "posts/$post->post_name" ) );
	}
	return $url;
}
add_filter( 'post_link', 'append_query_string', 10, 3 );
