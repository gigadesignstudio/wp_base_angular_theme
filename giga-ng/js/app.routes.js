App.config([
  '$locationProvider',
  '$stateProvider',
  '$urlRouterProvider',
  'cfpLoadingBarProvider',
  function ($locationProvider, $stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

    // Infinite Scrolling

    // $urlRouterProvider.deferIntercept();

    // Loading

    cfpLoadingBarProvider.includeSpinner = false;

    // Routing

    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/');


    // States

    $stateProvider.state('home', {
      url: "/?&page_id&post_type&p&preview",
      templateUrl: initialVars.templateUrl +"home/home.html",
      controller: 'homeController',
    });

    // Archive

    $stateProvider.state('archive', {
      url: "/:postType/",
      templateUrl: function (elem, attr) {
        // if (elem.postTypes == 'custom') {
        //   return initialVars.templateUrl + "archive/archive-custom.html";
        // } else { ...
        return initialVars.templateUrl +"archive/archive.html";
      },
      controllerProvider:['$stateParams', function ($stateParams) {
        // if ($stateParams.postTypes == 'custom') {
        //   return 'archiveCustomController';
        // } else { ...
        return 'archiveController';
      }],
    });

    // Single

    $stateProvider.state('single', {
      url: "/:postType/:elementSlug/?id&preview&preview_id&preview_nonce",
      templateUrl: function (elem, attr) {
        // if (elem.postTypes == 'custom') {
        //   return initialVars.templateUrl + "custom/single-custom.html";
        // } else { ...
        return initialVars.templateUrl +"single/single.html";
      },
      controllerProvider:['$stateParams', function ($stateParams) {
        // if ($stateParams.postTypes == 'custom') {
        //   return 'singleCustomController';
        // } else { ...
        return 'singleController';
      }],
    });

}]);
