var App = angular.module('gigaApp', ['ngRoute', 'ui.router', 'ngSanitize', 'ngAnimate', 'angular-loading-bar']);

App.factory('appState', ['$rootScope', function($rootScope) {
  return {
    internalCache: {
      archive: {
        event: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
        file: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
        diary: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
        page: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
      },
      single: {
        event: {},
        file: {},
        diary: {},
        page: {},
      }
    },

    checkIfInCache: function (type, postType, element) {
      if (type == 'archive') {
        return (this.internalCache[type][postType].currentPage >= element);
      }
      if (type == 'single') {
        if (this.internalCache[type][postType][element]) {
          return true;
        } else {
          return false;
        }
      }
    },

    getFromCache: function (type, postType, element) {
      if (type == 'archive') {
        return this.internalCache[type][postType];
      }
      if (type == 'single') {
        return this.internalCache[type][postType][element];
      }
    },

    addToCache: function (type, postType, element) {
      if (type == 'archive') {
        this.internalCache[type][postType] = element;
      }
      if (type == 'single') {
        this.internalCache[type][postType][element.slug] = element;
      }
    },

  };
}]);

App.config([
  '$locationProvider',
  '$stateProvider',
  '$urlRouterProvider',
  'cfpLoadingBarProvider',
  function ($locationProvider, $stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

    // Infinite Scrolling

    // $urlRouterProvider.deferIntercept();

    // Loading

    cfpLoadingBarProvider.includeSpinner = false;

    // Routing

    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/');


    // States

    $stateProvider.state('home', {
      url: "/?&page_id&post_type&p&preview",
      templateUrl: initialVars.templateUrl +"home/home.html",
      controller: 'homeController',
    });

    // Archive

    $stateProvider.state('archive', {
      url: "/:postType/",
      templateUrl: function (elem, attr) {
        // if (elem.postTypes == 'custom') {
        //   return initialVars.templateUrl + "archive/archive-custom.html";
        // } else { ...
        return initialVars.templateUrl +"archive/archive.html";
      },
      controllerProvider:['$stateParams', function ($stateParams) {
        // if ($stateParams.postTypes == 'custom') {
        //   return 'archiveCustomController';
        // } else { ...
        return 'archiveController';
      }],
    });

    // Single

    $stateProvider.state('single', {
      url: "/:postType/:elementSlug/?id&preview&preview_id&preview_nonce",
      templateUrl: function (elem, attr) {
        // if (elem.postTypes == 'custom') {
        //   return initialVars.templateUrl + "custom/single-custom.html";
        // } else { ...
        return initialVars.templateUrl +"single/single.html";
      },
      controllerProvider:['$stateParams', function ($stateParams) {
        // if ($stateParams.postTypes == 'custom') {
        //   return 'singleCustomController';
        // } else { ...
        return 'singleController';
      }],
    });

}]);

App.run(['$rootScope', 'appMainState', 'getData', function ($rootScope, appMainState, getData) {
  $rootScope.menus = initialVars.menus;
}]);

App.directive('backImg', ['$window', function ($window) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {

      scope.$watch(attrs.backImg, function(value) {
        if (value !== undefined) {
          element.css({
              'background-image': 'url(' + value +')',
          });

        }
      });
    },
  };
}]);

App.service('getData',['$http', '$location', '$timeout', 'appMainState', function ($http, $location, $timeout, appMainState) {

  this.getUrl = function (type, filters, params, id, revision) {

    var url = 'wp-json/wp/v2/';
    // if (endpoint) {
    //   url = 'wp-json/'+endpoint+'/v1/';
    // }
    var fUrl = '';
    var pUrl = '';
    if (type) {
      url = url + type + '/';
    }
    if (id) {
      url = url + id + '/';
    }
    if (revision) {
      url += 'revisions/?context=edit';
      // url = 'wp-json/wp/v2/pages/2/revisions/';
    } else {

      url += '?';
      if (filters) {
        for (var f in filters) {
          if (filters.hasOwnProperty(f)) {
            fUrl = fUrl+'filter['+f+']='+filters[f]+'&';
          }
        }
        url += fUrl;
      }
      if (params) {
        for (var p in params) {
          if (params.hasOwnProperty(p)) {
            pUrl = pUrl+p+'='+params[p]+'&';
          }
        }
        url += pUrl;
      }

    }

    console.log(url);
    return url;
  };

  this.getData = function (url) {
    var config = {
      headers: {
        'X-WP-Nonce': initialVars.wpApiSettings.nonce,
      }
    };
    return $http.get(url, config);
  };

  this.insertData = function (url, $scope, position, revision) {
    $scope.loading = true;
    this.getData(url)
    .then(function (success) {
      $scope.loading = false;
      if ($scope.$root) {
        $scope.$root.infiniteLoading = false;
      }
      $scope[position+'Loaded'] = true;
      if (revision) {
        $scope[position] = $scope[position].concat(success.data[0]);        
      } else {
        $scope[position] = $scope[position].concat(success.data);
      }
      // $scope.WpTotal = headers('X-WP-Total');
      // $scope.WpTotalPages = headers('X-WP-TotalPages');
    }, function (error) {
      console.log(error);
    });
  };

  this.insertContent = function (getParams, $scope, position, revision) {

    var type = getParams.type;
    var filters = getParams.filters;
    var params = getParams.params;
    var id = getParams.id;

    // Set Vars
    if (!filters) {
      filters = {};
    }
    if (!params) {
      params = {};
    }

    // Set Default Params
    if (!(params.slug || id)) {
      // Is an archive
      if (!params.page) {
        params.page = 1;
      }
      if (!params.per_page) {
        params.per_page = 20;
      }
    }

    // Get Url
    var url = this.getUrl(type, filters, params, id, revision);
    this.insertData(url, $scope, position, revision);

  };

  // this.insertArticleInArchive = function (postType, page, $scope, position) {
  //   var filters = null;
  //   var params = {
  //     'page': page,
  //     'per_page': 20,
  //   };
  //   var url = this.getUrl(postType, false, filters, params);
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertArticleInHome = function (postType, page, $scope, position) {
  //   var filters = null;
  //   var params = {
  //     'page': page,
  //     'per_page': 20,
  //   };
  //   var url = this.getUrl(postType, false, filters, params, 'home');
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertLastArticleInArchive = function (postType, $scope, position) {
  //   var currentParams = {
  //     per_page: 1,
  //   };
  //   var url = this.getUrl(postType, false, false, currentParams);
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertArticleInSingle = function ($stateParams, $scope, position) {
  //   var currentParams = {
  //     slug: $stateParams.elementSlug
  //   };
  //   var url = this.getUrl($stateParams.postType, false, false, currentParams);
  //   // this.insertData(url, $scope, position);
  // };
  //
  // this.insertArticleInArchiveByCat = function ($stateParams, taxonomy, $scope, position) {
  //   var currentFilter = {};
  //   currentFilter[taxonomy] = $stateParams.t;
  //   var url = this.getUrl($stateParams.mainType, false, currentFilter);
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertUsersInArchive = function (metaValue, $scope, position) {
  //   var currentParams = {
  //     meta_key: 'istituzione',
  //     meta_value: metaValue
  //   };
  //   var url = this.getUrl('users', false, false, currentParams);
  //   this.insertData(url, $scope, position);
  // };

}]);

App.directive('infiniteScroll', ['getData', '$window', '$rootScope', '$location', '$timeout', function(getData, $window, $rootScope, $location, $timeout) {
  return function(scope, element, attrs) {

      angular.element(window).bind("scroll", function() {

        // console.log('bind infinite');

        var deltaScroll = 100;
        var bottomWindowHeight = angular.element(window).scrollTop() + window.outerHeight + deltaScroll;
        var elementHeight = element.outerHeight(true);
        var scrollType = attrs.infiniteScroll;

        // Call next Page

        if (!$rootScope.infiniteLoading) {
          if (bottomWindowHeight > elementHeight) {
            $rootScope.infiniteLoading = true;
            scope.getNextPage();
          }
        }

      });

      // Unbind Scroll

      // $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      //
      //   console.log('unbind scroll');
      //
      //   angular.element($window).unbind('scroll');
      //   $rootScope.infiniteLoading = false;
      //
      // });

    };
}]);

App.directive('infiniteScrollDiv', ['getData', '$window', '$rootScope', '$location', '$timeout', function(getData, $window, $rootScope, $location, $timeout) {
  return function(scope, element, attrs) {

    var elementScrollingId = '#'+element.context.id;
    var elementScopeId = '#'+attrs.infiniteScrollDiv;

      angular.element(elementScrollingId).bind("scroll", function() {

        var deltaScroll = 100;
        var bottomWindowHeight = angular.element(elementScrollingId).scrollTop() + angular.element(elementScrollingId).height() + deltaScroll;
        var elementHeight = angular.element(elementScrollingId).height();
        var scrollType = attrs.infiniteScroll;

        // Call next Page

        if (!$rootScope.infiniteLoading) {
          if (bottomWindowHeight > elementHeight) {
            $rootScope.infiniteLoading = true;
            if (angular.element(elementScopeId).scope()) {
              angular.element(elementScopeId).scope().getNextPage();
            }
          }
        }

      });

      // Unbind Scroll

      // $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      //
      //   console.log('unbind scroll');
      //
      //   angular.element($window).unbind('scroll');
      //   $rootScope.infiniteLoading = false;
      //
      // });

    };
}]);

App.controller('mainCtrl', ['$scope', 'appMainState', '$rootScope', '$location', function ($scope, appMainState, $rootScope, $location) {

  $rootScope.initialVars = initialVars;

  $rootScope.getBEMclass = function (block, element, modifier) {
    if (element) {
      if (modifier) {
        return block+"__"+element+"--"+modifier;
      }
      return block+"__"+element;
    } else {
      if (modifier) {
        return block+"--"+modifier;
      }
      return block;
    }
  };

}]);

App.factory('appMainState', ['$rootScope', function($rootScope) {
  return {
    data: true,
  };
}]);

App.directive('resImg', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
  return {
    restrict: 'A',
    scope: {
      resImg: '=',
    },
    link: function(scope, element, attrs) {

      function isSliderBigger(m, pM) {
        var pMs, Ms;
        if (pM === undefined) {
          return true;
        } else {
          for (var i = 0; i < initialVars.imagesSizes.length; i++) {
            if (initialVars.imagesSizes[i] == pM) {
              pMs = i;
            }
            if (initialVars.imagesSizes[i] == m) {
              Ms = i;
            }
          }
          return pMs < Ms;
        }
      }

      function setElementSrc (scope, $timeout) {
        var method = scope.resImg.method;
        var mElement = scope.resImg.with;
        var currentImg = scope.resImg.img;
        var destinationVar = scope.resImg.destination;
        var elSize, mElSize;
        var measure = false;
        var prevMeasure = false;

        if (currentImg) {
          $timeout(function () {
            for (var i = 0; i < initialVars.imagesSizes.length; i++) {
              if (method == 'w') {
                elSize = currentImg.sizes[initialVars.imagesSizes[i]+'-width'];
                mElSize = angular.element('#'+mElement).width();
              }
              if (method == 'h') {
                elSize = currentImg.sizes[initialVars.imagesSizes[i]+'-height'];
                mElSize = angular.element('#'+mElement).height();
              }
              if (initialVars.imagesSizes[i] == 'original' && measure === false) {  // biggest one
                measure = 'original';
              } else {
                if (measure === false && (elSize > mElSize)) {
                  measure = initialVars.imagesSizes[i];
                }
              }
            }

            prevMeasure = scope.rImgSize;
            if (prevMeasure != measure && isSliderBigger(measure, prevMeasure)) {
              scope.rImgSize = measure;
              if (measure == 'original') {
                imageUrl = currentImg.url;
              } else {
                imageUrl = currentImg.sizes[measure];
              }
              scope.imageUrl = imageUrl;
              scope.$parent[destinationVar] = imageUrl;
            }

          }, 100);
        }

      }

      // First setting

      setElementSrc(scope, $timeout);
      var prevSize = 0;

      // // Bind Resize

      angular.element($window).bind('resize', function(){
          setElementSrc(scope, $timeout);
       });

      },
  };
}]);

App.controller('archiveController', ['$stateParams', '$scope', 'getData', function($stateParams, $scope, getData) {

    // Var
    $scope.$stateParams = $stateParams;
    $scope.articles = Array();
    $scope.nextPage = 1;

    // Fn

    $scope.getNextPage = function () {
      var params = {};
      var filters = {};
      if ($scope.nextPage) {
        var getParams = {
          type: $scope.$stateParams.postType,
          filters: filters,
          params: params,
          id: null
        };

        getData.insertContent(getParams, $scope, 'articles');
      }
    };

    // Begin!

    $scope.getNextPage();

    // Watch
}]);

App.directive('header', function() {
  return {
    restrict: 'E',
    scope: {
      id: '@',
    },
    templateUrl: initialVars.templateUrl + 'header/header.html',
    controller: ['$scope', '$location', '$rootScope', 'getData', function ($scope, $location, $rootScope, getData) {

      // Var
      // Fn
      // Begin!
      // Watch

    }],
    link: function(scope, element, attrs) {

     },
  };
});

App.controller('homeController', ['$stateParams', '$scope', 'getData', '$state', function($stateParams, $scope, getData, $state) {

  // Var
  $scope.$stateParams = $stateParams;

  if ($stateParams.preview == 'true') {
    // Post
    if ($stateParams.p) {
      if ($stateParams.post_type) {
        $state.go('single', {postType: $stateParams.post_type, elementSlug: 'preview', id: $stateParams.p, preview: 'true'});
      } else {
        $state.go('single', {postType: 'posts', elementSlug: 'preview', id: $stateParams.p, preview: 'true'});
      }
    }
    // Page
    if ($stateParams.page_id) {
      $state.go('single', {postType: 'pages', elementSlug: 'preview', id: $stateParams.page_id, preview: 'true'});
    }
  }

  // Fn

  // Begin!

  // Watch

}]);

function setMenuClass(scope, url) {
  if (scope.menu) {
    for (var i = 0; i < scope.menu.length; i++) {
      if (scope.menu[i].type == "post_type_archive") {
        if (url.indexOf(scope.menu[i].url)!=-1) {
          scope.menu[i].class = "current-section";
        } else {
          scope.menu[i].class = false;
        }
      }
      if (scope.menu[i].type == "post_type") {
        if (scope.menu[i].url == url) {
          scope.menu[i].class = "current-page";
        } else {
          scope.menu[i].class = false;
        }
      }
    }
  }
}

App.directive('menuContent', function() {
  return {
    restrict: 'A',
    scope: {
      id: '@',
    },
    templateUrl: initialVars.templateUrl + 'menu/menu.html',
    controller: ['$scope', '$location', '$rootScope', function ($scope, $location, $rootScope) {
      $scope.menu = $rootScope.menus[$scope.id];
      // if ($scope.$parent.section) { // main case
      //   $scope.menu = $scope.$parent.section.menu;
      // } else {
      //   $scope.menu = $scope.side.menu;
      // }
      // setMenuClass($scope, $location.absUrl());
    }],
    link: function(scope, element, attrs) {
        //  scope.$on('$locationChangeSuccess', function(next, current) {
        //    setMenuClass(scope, current);
        //  });
     },
  };
});

App.controller('singleController', ['$stateParams', '$scope', 'getData', function($stateParams, $scope, getData) {

  // Var
  $scope.$stateParams = $stateParams;
  $scope.articles = Array();
  $scope.revision = false;

  // Fn

  $scope.getNextPage = function () {

    // Preview from wp-admin
    if ($scope.$stateParams.preview == 'true') {
      if (!$scope.$stateParams.preview_id) {
        // Draft
        delete($scope.$stateParams.elementSlug);
      } else {
        // Autosave
        delete($scope.$stateParams.elementSlug);
        $scope.revision = true;
        $scope.$stateParams.id = $scope.$stateParams.preview_id;
      }
    }

    var params = {};
    var filters = {};
    params.slug = $scope.$stateParams.elementSlug;

    var getParams = {
      type: $scope.$stateParams.postType,
      filters: filters,
      params: params,
      id: $scope.$stateParams.id
    };

    getData.insertContent(getParams, $scope, 'articles', $scope.revision);

  };

  // Begin!

  $scope.getNextPage();

  // Watch

}]);
