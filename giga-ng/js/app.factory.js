App.factory('appState', ['$rootScope', function($rootScope) {
  return {
    internalCache: {
      archive: {
        event: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
        file: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
        diary: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
        page: {
          WpTotalPages: 1,
          currentPage: 0,
          elements: []
        },
      },
      single: {
        event: {},
        file: {},
        diary: {},
        page: {},
      }
    },

    checkIfInCache: function (type, postType, element) {
      if (type == 'archive') {
        return (this.internalCache[type][postType].currentPage >= element);
      }
      if (type == 'single') {
        if (this.internalCache[type][postType][element]) {
          return true;
        } else {
          return false;
        }
      }
    },

    getFromCache: function (type, postType, element) {
      if (type == 'archive') {
        return this.internalCache[type][postType];
      }
      if (type == 'single') {
        return this.internalCache[type][postType][element];
      }
    },

    addToCache: function (type, postType, element) {
      if (type == 'archive') {
        this.internalCache[type][postType] = element;
      }
      if (type == 'single') {
        this.internalCache[type][postType][element.slug] = element;
      }
    },

  };
}]);
