App.directive('infiniteScroll', ['getData', '$window', '$rootScope', '$location', '$timeout', function(getData, $window, $rootScope, $location, $timeout) {
  return function(scope, element, attrs) {

      angular.element(window).bind("scroll", function() {

        // console.log('bind infinite');

        var deltaScroll = 100;
        var bottomWindowHeight = angular.element(window).scrollTop() + window.outerHeight + deltaScroll;
        var elementHeight = element.outerHeight(true);
        var scrollType = attrs.infiniteScroll;

        // Call next Page

        if (!$rootScope.infiniteLoading) {
          if (bottomWindowHeight > elementHeight) {
            $rootScope.infiniteLoading = true;
            scope.getNextPage();
          }
        }

      });

      // Unbind Scroll

      // $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      //
      //   console.log('unbind scroll');
      //
      //   angular.element($window).unbind('scroll');
      //   $rootScope.infiniteLoading = false;
      //
      // });

    };
}]);
