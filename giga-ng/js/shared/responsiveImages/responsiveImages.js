App.directive('resImg', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
  return {
    restrict: 'A',
    scope: {
      resImg: '=',
    },
    link: function(scope, element, attrs) {

      function isSliderBigger(m, pM) {
        var pMs, Ms;
        if (pM === undefined) {
          return true;
        } else {
          for (var i = 0; i < initialVars.imagesSizes.length; i++) {
            if (initialVars.imagesSizes[i] == pM) {
              pMs = i;
            }
            if (initialVars.imagesSizes[i] == m) {
              Ms = i;
            }
          }
          return pMs < Ms;
        }
      }

      function setElementSrc (scope, $timeout) {
        var method = scope.resImg.method;
        var mElement = scope.resImg.with;
        var currentImg = scope.resImg.img;
        var destinationVar = scope.resImg.destination;
        var elSize, mElSize;
        var measure = false;
        var prevMeasure = false;

        if (currentImg) {
          $timeout(function () {
            for (var i = 0; i < initialVars.imagesSizes.length; i++) {
              if (method == 'w') {
                elSize = currentImg.sizes[initialVars.imagesSizes[i]+'-width'];
                mElSize = angular.element('#'+mElement).width();
              }
              if (method == 'h') {
                elSize = currentImg.sizes[initialVars.imagesSizes[i]+'-height'];
                mElSize = angular.element('#'+mElement).height();
              }
              if (initialVars.imagesSizes[i] == 'original' && measure === false) {  // biggest one
                measure = 'original';
              } else {
                if (measure === false && (elSize > mElSize)) {
                  measure = initialVars.imagesSizes[i];
                }
              }
            }

            prevMeasure = scope.rImgSize;
            if (prevMeasure != measure && isSliderBigger(measure, prevMeasure)) {
              scope.rImgSize = measure;
              if (measure == 'original') {
                imageUrl = currentImg.url;
              } else {
                imageUrl = currentImg.sizes[measure];
              }
              scope.imageUrl = imageUrl;
              scope.$parent[destinationVar] = imageUrl;
            }

          }, 100);
        }

      }

      // First setting

      setElementSrc(scope, $timeout);
      var prevSize = 0;

      // // Bind Resize

      angular.element($window).bind('resize', function(){
          setElementSrc(scope, $timeout);
       });

      },
  };
}]);
