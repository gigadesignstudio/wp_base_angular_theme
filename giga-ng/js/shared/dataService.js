App.service('getData',['$http', '$location', '$timeout', 'appMainState', function ($http, $location, $timeout, appMainState) {

  this.getUrl = function (type, filters, params, id, revision) {

    var url = 'wp-json/wp/v2/';
    // if (endpoint) {
    //   url = 'wp-json/'+endpoint+'/v1/';
    // }
    var fUrl = '';
    var pUrl = '';
    if (type) {
      url = url + type + '/';
    }
    if (id) {
      url = url + id + '/';
    }
    if (revision) {
      url += 'revisions/?context=edit';
      // url = 'wp-json/wp/v2/pages/2/revisions/';
    } else {

      url += '?';
      if (filters) {
        for (var f in filters) {
          if (filters.hasOwnProperty(f)) {
            fUrl = fUrl+'filter['+f+']='+filters[f]+'&';
          }
        }
        url += fUrl;
      }
      if (params) {
        for (var p in params) {
          if (params.hasOwnProperty(p)) {
            pUrl = pUrl+p+'='+params[p]+'&';
          }
        }
        url += pUrl;
      }

    }

    console.log(url);
    return url;
  };

  this.getData = function (url) {
    var config = {
      headers: {
        'X-WP-Nonce': initialVars.wpApiSettings.nonce,
      }
    };
    return $http.get(url, config);
  };

  this.insertData = function (url, $scope, position, revision) {
    $scope.loading = true;
    this.getData(url)
    .then(function (success) {
      $scope.loading = false;
      if ($scope.$root) {
        $scope.$root.infiniteLoading = false;
      }
      $scope[position+'Loaded'] = true;
      if (revision) {
        $scope[position] = $scope[position].concat(success.data[0]);        
      } else {
        $scope[position] = $scope[position].concat(success.data);
      }
      // $scope.WpTotal = headers('X-WP-Total');
      // $scope.WpTotalPages = headers('X-WP-TotalPages');
    }, function (error) {
      console.log(error);
    });
  };

  this.insertContent = function (getParams, $scope, position, revision) {

    var type = getParams.type;
    var filters = getParams.filters;
    var params = getParams.params;
    var id = getParams.id;

    // Set Vars
    if (!filters) {
      filters = {};
    }
    if (!params) {
      params = {};
    }

    // Set Default Params
    if (!(params.slug || id)) {
      // Is an archive
      if (!params.page) {
        params.page = 1;
      }
      if (!params.per_page) {
        params.per_page = 20;
      }
    }

    // Get Url
    var url = this.getUrl(type, filters, params, id, revision);
    this.insertData(url, $scope, position, revision);

  };

  // this.insertArticleInArchive = function (postType, page, $scope, position) {
  //   var filters = null;
  //   var params = {
  //     'page': page,
  //     'per_page': 20,
  //   };
  //   var url = this.getUrl(postType, false, filters, params);
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertArticleInHome = function (postType, page, $scope, position) {
  //   var filters = null;
  //   var params = {
  //     'page': page,
  //     'per_page': 20,
  //   };
  //   var url = this.getUrl(postType, false, filters, params, 'home');
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertLastArticleInArchive = function (postType, $scope, position) {
  //   var currentParams = {
  //     per_page: 1,
  //   };
  //   var url = this.getUrl(postType, false, false, currentParams);
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertArticleInSingle = function ($stateParams, $scope, position) {
  //   var currentParams = {
  //     slug: $stateParams.elementSlug
  //   };
  //   var url = this.getUrl($stateParams.postType, false, false, currentParams);
  //   // this.insertData(url, $scope, position);
  // };
  //
  // this.insertArticleInArchiveByCat = function ($stateParams, taxonomy, $scope, position) {
  //   var currentFilter = {};
  //   currentFilter[taxonomy] = $stateParams.t;
  //   var url = this.getUrl($stateParams.mainType, false, currentFilter);
  //   this.insertData(url, $scope, position);
  // };
  //
  // this.insertUsersInArchive = function (metaValue, $scope, position) {
  //   var currentParams = {
  //     meta_key: 'istituzione',
  //     meta_value: metaValue
  //   };
  //   var url = this.getUrl('users', false, false, currentParams);
  //   this.insertData(url, $scope, position);
  // };

}]);
