App.directive('backImg', ['$window', function ($window) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {

      scope.$watch(attrs.backImg, function(value) {
        if (value !== undefined) {
          element.css({
              'background-image': 'url(' + value +')',
          });

        }
      });
    },
  };
}]);
