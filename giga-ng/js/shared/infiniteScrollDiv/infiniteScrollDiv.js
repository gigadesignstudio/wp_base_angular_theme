App.directive('infiniteScrollDiv', ['getData', '$window', '$rootScope', '$location', '$timeout', function(getData, $window, $rootScope, $location, $timeout) {
  return function(scope, element, attrs) {

    var elementScrollingId = '#'+element.context.id;
    var elementScopeId = '#'+attrs.infiniteScrollDiv;

      angular.element(elementScrollingId).bind("scroll", function() {

        var deltaScroll = 100;
        var bottomWindowHeight = angular.element(elementScrollingId).scrollTop() + angular.element(elementScrollingId).height() + deltaScroll;
        var elementHeight = angular.element(elementScrollingId).height();
        var scrollType = attrs.infiniteScroll;

        // Call next Page

        if (!$rootScope.infiniteLoading) {
          if (bottomWindowHeight > elementHeight) {
            $rootScope.infiniteLoading = true;
            if (angular.element(elementScopeId).scope()) {
              angular.element(elementScopeId).scope().getNextPage();
            }
          }
        }

      });

      // Unbind Scroll

      // $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      //
      //   console.log('unbind scroll');
      //
      //   angular.element($window).unbind('scroll');
      //   $rootScope.infiniteLoading = false;
      //
      // });

    };
}]);
