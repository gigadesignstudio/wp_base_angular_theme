App.controller('mainCtrl', ['$scope', 'appMainState', '$rootScope', '$location', function ($scope, appMainState, $rootScope, $location) {

  $rootScope.initialVars = initialVars;

  $rootScope.getBEMclass = function (block, element, modifier) {
    if (element) {
      if (modifier) {
        return block+"__"+element+"--"+modifier;
      }
      return block+"__"+element;
    } else {
      if (modifier) {
        return block+"--"+modifier;
      }
      return block;
    }
  };

}]);
