App.controller('homeController', ['$stateParams', '$scope', 'getData', '$state', function($stateParams, $scope, getData, $state) {

  // Var
  $scope.$stateParams = $stateParams;

  if ($stateParams.preview == 'true') {
    // Post
    if ($stateParams.p) {
      if ($stateParams.post_type) {
        $state.go('single', {postType: $stateParams.post_type, elementSlug: 'preview', id: $stateParams.p, preview: 'true'});
      } else {
        $state.go('single', {postType: 'posts', elementSlug: 'preview', id: $stateParams.p, preview: 'true'});
      }
    }
    // Page
    if ($stateParams.page_id) {
      $state.go('single', {postType: 'pages', elementSlug: 'preview', id: $stateParams.page_id, preview: 'true'});
    }
  }

  // Fn

  // Begin!

  // Watch

}]);
