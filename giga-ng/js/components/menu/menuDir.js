function setMenuClass(scope, url) {
  if (scope.menu) {
    for (var i = 0; i < scope.menu.length; i++) {
      if (scope.menu[i].type == "post_type_archive") {
        if (url.indexOf(scope.menu[i].url)!=-1) {
          scope.menu[i].class = "current-section";
        } else {
          scope.menu[i].class = false;
        }
      }
      if (scope.menu[i].type == "post_type") {
        if (scope.menu[i].url == url) {
          scope.menu[i].class = "current-page";
        } else {
          scope.menu[i].class = false;
        }
      }
    }
  }
}

App.directive('menuContent', function() {
  return {
    restrict: 'A',
    scope: {
      id: '@',
    },
    templateUrl: initialVars.templateUrl + 'menu/menu.html',
    controller: ['$scope', '$location', '$rootScope', function ($scope, $location, $rootScope) {
      $scope.menu = $rootScope.menus[$scope.id];
      // if ($scope.$parent.section) { // main case
      //   $scope.menu = $scope.$parent.section.menu;
      // } else {
      //   $scope.menu = $scope.side.menu;
      // }
      // setMenuClass($scope, $location.absUrl());
    }],
    link: function(scope, element, attrs) {
        //  scope.$on('$locationChangeSuccess', function(next, current) {
        //    setMenuClass(scope, current);
        //  });
     },
  };
});
