App.controller('singleController', ['$stateParams', '$scope', 'getData', function($stateParams, $scope, getData) {

  // Var
  $scope.$stateParams = $stateParams;
  $scope.articles = Array();
  $scope.revision = false;

  // Fn

  $scope.getNextPage = function () {

    // Preview from wp-admin
    if ($scope.$stateParams.preview == 'true') {
      if (!$scope.$stateParams.preview_id) {
        // Draft
        delete($scope.$stateParams.elementSlug);
      } else {
        // Autosave
        delete($scope.$stateParams.elementSlug);
        $scope.revision = true;
        $scope.$stateParams.id = $scope.$stateParams.preview_id;
      }
    }

    var params = {};
    var filters = {};
    params.slug = $scope.$stateParams.elementSlug;

    var getParams = {
      type: $scope.$stateParams.postType,
      filters: filters,
      params: params,
      id: $scope.$stateParams.id
    };

    getData.insertContent(getParams, $scope, 'articles', $scope.revision);

  };

  // Begin!

  $scope.getNextPage();

  // Watch

}]);
