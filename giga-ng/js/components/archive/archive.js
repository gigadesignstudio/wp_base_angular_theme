App.controller('archiveController', ['$stateParams', '$scope', 'getData', function($stateParams, $scope, getData) {

    // Var
    $scope.$stateParams = $stateParams;
    $scope.articles = Array();
    $scope.nextPage = 1;

    // Fn

    $scope.getNextPage = function () {
      var params = {};
      var filters = {};
      if ($scope.nextPage) {
        var getParams = {
          type: $scope.$stateParams.postType,
          filters: filters,
          params: params,
          id: null
        };

        getData.insertContent(getParams, $scope, 'articles');
      }
    };

    // Begin!

    $scope.getNextPage();

    // Watch
}]);
