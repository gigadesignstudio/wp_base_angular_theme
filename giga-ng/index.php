<!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="gigaApp" ng-controller="mainCtrl">
<head>
	<base href="/<?php echo basename(site_url()); ?>/">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<!--
	gigadesignstudio.com
	-->
</head>

<body <?php body_class(); ?>>

	<header></header>

  <main ui-view>
  </main>

  <footer id="colophon" class="site-footer" role="contentinfo">
  </footer>
  <!-- .site-footer -->

<?php wp_footer(); ?>
</body>
</html>
