<?php

if ( ! function_exists( 'pablone_setup' ) ) :
function pablone_setup() {

	register_nav_menus( array(
		'mainMenu' => __( 'Menu Principale', 'pablone' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

}
endif; // pablone_setup
add_action( 'after_setup_theme', 'pablone_setup' );

function get_simple_menu_obj ($menu_name) {
	$menu = wp_get_nav_menu_items($menu_name);
	$menu_simple = array();
	$page_count = 1;
	// Set Parent
	for ($i=0; $i < count($menu); $i++) {
		$current_menu_element['ID'] = $menu[$i]->ID;
		$current_menu_element['title'] = $menu[$i]->title;
		$current_menu_element['url'] = $menu[$i]->url;
		$current_menu_element['type'] = $menu[$i]->type;
		$current_menu_element['menu_item_parent'] = $menu[$i]->menu_item_parent;
		$current_menu_element['linkID'] = $menu[$i]->object_id;
		$current_menu_element['object'] = $menu[$i]->object;
		$menu_classes = "";
		for ($j=0; $j < count($menu[$i]->classes); $j++) {
			$menu_classes .= " ".$menu[$i]->classes[$j];
		}
		$current_menu_element['classes'] = $menu_classes;
		if ($menu[$i]->type == 'post_type') {
			$current_menu_element['number'] = $page_count;
			$page_count++;
		}
		if ($current_menu_element['menu_item_parent'] == 0) {
			$menu_simple[] = $current_menu_element;
		} else {
			$submenus[$current_menu_element['menu_item_parent']][] = $current_menu_element;
		}
	}
	// Set Children
	for ($i=0; $i < count($menu_simple); $i++) {
		$current_submenu_id = $menu_simple[$i]['ID'];
		if (!empty($submenus[$current_submenu_id])) {
			$menu_simple[$i]['submenu'] = $submenus[$current_submenu_id];
		}
	}
	return $menu_simple;
}

function pablone_scripts() {

	wp_enqueue_style( 'pablone-style', get_stylesheet_uri() );

	wp_register_script('angularjs', get_template_directory_uri() . '/bower_components/angular/angular.min.js');
	wp_register_script('angularjs-route', get_template_directory_uri() . '/bower_components/angular-route/angular-route.min.js');
	wp_register_script('angularjs-ui-router', get_template_directory_uri() . '/bower_components/angular-ui-router/release/angular-ui-router.min.js');
	wp_register_script('angularjs-sanitize', get_template_directory_uri() . '/bower_components/angular-sanitize/angular-sanitize.min.js');
	wp_register_script('angularjs-animate', get_template_directory_uri() . '/bower_components/angular-animate/angular-animate.min.js');
	wp_register_script('angularjs-loading-bar',	get_template_directory_uri() . '/bower_components/angular-loading-bar/build/loading-bar.min.js');

	wp_enqueue_script(
		'pablone-script',
		get_template_directory_uri() . '/js/functions.min.js',
		array(
			'jquery',
			'angularjs',
			'angularjs-route',
			'angularjs-ui-router',
			'angularjs-sanitize',
			'angularjs-animate',
			'angularjs-loading-bar',
		),
		'20170308',
		true
	);

	wp_enqueue_style('angularjs-loading-bar-css', get_template_directory_uri() . '/bower_components/angular-loading-bar/build/loading-bar.min.css');

	$images_size = array('post-thumbnail', 'mobile', 'medium', 'large', 'larger', 'wide', 'giant', 'extra', 'original');

	wp_localize_script( 'pablone-script', 'initialVars', array(
		'homeUrl'				=> get_home_url(),
		'templateUrl'		=> get_template_directory_uri().'/js/components/',
		'templateDirUrl'		=> get_template_directory_uri(),
		'siteTitle'			=> get_bloginfo( 'name' ),
		'imagesSizes'		=> $images_size,
		'menus'					=> array(
															'mainMenu' => get_simple_menu_obj('mainMenu'),
														),
		'wpApiSettings' => array(
			'root' => esc_url_raw( rest_url() ),
			'nonce' => wp_create_nonce( 'wp_rest' )
		),
	) );

}
add_action( 'wp_enqueue_scripts', 'pablone_scripts' );

/*
*	Remove emoji
*/

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  // add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

/*
 *	Deregister WP embed
 */

function deregister_wp_embed(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'deregister_wp_embed' );

?>
