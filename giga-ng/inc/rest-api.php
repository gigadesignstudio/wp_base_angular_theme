<?php
/*
 *	Rest API: show users even if no post
 */

add_filter( 'rest_user_query' , 'custom_rest_user_query', 10, 2 );

function custom_rest_user_query( $prepared_args, $request = null ) {

	// Show all users, even if have no pubblications
	unset($prepared_args['has_published_posts']);

	// error_log(print_R($request, true));
	$query_params = $request -> get_query_params();
	if ($request['meta_key']) {
		$prepared_args['meta_key'] = $request['meta_key'];
		$prepared_args['meta_value'] = $request['meta_value'];
	}
  return $prepared_args;
}

add_action( 'after_setup_theme', 'wnd_default_image_settings' );

function wnd_default_image_settings() {
	update_option( 'image_default_align', 'none' );
	update_option( 'image_default_link_type', 'none' );
	update_option( 'image_default_size', 'large' );
}

/*
 * 	Rest Api: Better Featured Image, by Braad Martin
 */


 add_action( 'init', 'better_rest_api_featured_images_init', 12 );
 /**
  * Register our enhanced better_featured_image field to all public post types
  * that support post thumbnails.
  *
  * @since  1.0.0
  */
 function better_rest_api_featured_images_init() {

 	$post_types = get_post_types( array( 'public' => true ), 'objects' );

 	foreach ( $post_types as $post_type ) {

 		$post_type_name     = $post_type->name;
 		$show_in_rest       = ( isset( $post_type->show_in_rest ) && $post_type->show_in_rest ) ? true : false;
 		$supports_thumbnail = post_type_supports( $post_type_name, 'thumbnail' );

 		// Only proceed if the post type is set to be accessible over the REST API
 		// and supports featured images.
 		if ( $show_in_rest && $supports_thumbnail ) {

 			// Compatibility with the REST API v2 beta 9+
 			if ( function_exists( 'register_rest_field' ) ) {
 				register_rest_field( $post_type_name,
 					'better_featured_image',
 					array(
 						'get_callback' => 'better_rest_api_featured_images_get_field',
 						'schema'       => null,
 					)
 				);
 			} elseif ( function_exists( 'register_api_field' ) ) {
 				register_api_field( $post_type_name,
 					'better_featured_image',
 					array(
 						'get_callback' => 'better_rest_api_featured_images_get_field',
 						'schema'       => null,
 					)
 				);
 			}
 		}
 	}
 }

 /**
  * Return the better_featured_image field.
  *
  * @since   1.0.0
  *
  * @param   object  $object      The response object.
  * @param   string  $field_name  The name of the field to add.
  * @param   object  $request     The WP_REST_Request object.
  *
  * @return  object|null
  */
 function better_rest_api_featured_images_get_field( $object, $field_name, $request ) {

 	// Only proceed if the post has a featured image.
 	if ( ! empty( $object['featured_media'] ) ) {
 		$image_id = (int)$object['featured_media'];
 	} elseif ( ! empty( $object['featured_image'] ) ) {
 		$image_id = (int)$object['featured_image'];
 	} else {
 		return null;
 	}

 	$image = get_post( $image_id );

 	if ( ! $image ) {
 		return null;
 	}

 	// This is taken from WP_REST_Attachments_Controller::prepare_item_for_response().
 	$featured_image['id']            = $image_id;
 	$featured_image['alt_text']      = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
 	$featured_image['caption']       = $image->post_excerpt;
 	$featured_image['description']   = $image->post_content;
 	$featured_image['media_type']    = wp_attachment_is_image( $image_id ) ? 'image' : 'file';
 	$featured_image['media_details'] = wp_get_attachment_metadata( $image_id );
 	$featured_image['post']          = ! empty( $image->post_parent ) ? (int) $image->post_parent : null;
 	$featured_image['source_url']    = wp_get_attachment_url( $image_id );

 	if ( empty( $featured_image['media_details'] ) ) {
 		$featured_image['media_details'] = new stdClass;
 	} elseif ( ! empty( $featured_image['media_details']['sizes'] ) ) {
 		$img_url_basename = wp_basename( $featured_image['source_url'] );
 		foreach ( $featured_image['media_details']['sizes'] as $size => &$size_data ) {
 			$image_src = wp_get_attachment_image_src( $image_id, $size );
 			if ( ! $image_src ) {
 				continue;
 			}
 			$size_data['source_url'] = $image_src[0];
 		}
 	} else {
 		$featured_image['media_details']['sizes'] = new stdClass;
 	}

 	return apply_filters( 'better_rest_api_featured_image', $featured_image, $image_id );
 }

/*
 * Add ACF to Rest Api Revisions
 */

function acf_revision_support($types) {
    $types['revision'] = 'revision';
    $types['draft'] = 'draft';
    return $types;
}

add_filter('acf/rest_api/types', 'acf_revision_support');



?>
