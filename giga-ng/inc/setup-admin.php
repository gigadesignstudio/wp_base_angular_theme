<?php

/*
 *	Admin Bar
 */

show_admin_bar( false );

/*
 *	Admin Extra Sanitize Title
 */

add_filter( 'sanitize_title', 'sanitize_title_extra' );

function sanitize_title_extra( $title ) {

    $friendlyURL = htmlentities($title, ENT_COMPAT, "UTF-8", false);
    $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
    $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8");
    $friendlyURL = preg_replace('/[^a-z0-9-]+/i', '-', $friendlyURL);
    $friendlyURL = preg_replace('/-+/', '-', $friendlyURL);
    $friendlyURL = trim($friendlyURL, '-');
    $friendlyURL = strtolower($friendlyURL);
    return $friendlyURL;

}

/*
 *	Add Custom CSS
 */

function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

?>
