<?php

/*
Change the default image settings

 mobile: 				max-width: 300
 medium:					max-width: 600
 large:					max-width: 800
 larger:					max-width: 1200
 wide:						max-width: 1500
 giant:					max-width: 2000
 extra:					max-width: 3000
 Original:		-
*/

add_action( 'after_setup_theme', 'pablone_admin_images' );
function pablone_admin_images() {
  add_image_size( 'mobile', 300, 9999);
  add_image_size( 'larger', 1200, 9999);
  add_image_size( 'wide', 1500, 9999);
  add_image_size( 'giant', 2000, 9999);
  add_image_size( 'extra', 3000, 9999);
}


/**
 * 	Disable responsive image support
 */

// Clean the up the image from wp_get_attachment_image()
add_filter( 'wp_get_attachment_image_attributes', function( $attr )
{
    if( isset( $attr['sizes'] ) )
        unset( $attr['sizes'] );

    if( isset( $attr['srcset'] ) )
        unset( $attr['srcset'] );

    return $attr;

 }, PHP_INT_MAX );

// Override the calculated image sizes
add_filter( 'wp_calculate_image_sizes', '__return_false',  PHP_INT_MAX );

// Override the calculated image sources
add_filter( 'wp_calculate_image_srcset', '__return_false', PHP_INT_MAX );

// Remove the reponsive stuff from the content
remove_filter( 'the_content', 'wp_make_content_images_responsive' );

/*
 * 	Rename Uploaded Images with Post Name
 */

 add_filter('wp_handle_upload_prefilter', 'custom_upload_filter' );

 function custom_upload_filter( $file ){
	 global $post;

	 $site_name = sanitize_title(get_bloginfo( 'name' ))."_";
	 $file_type = wp_check_filetype($file['name']);
	 $file_ext = '.'.$file_type['ext'];
	 $file_name = basename($file['name'], $file_ext);

	 if ( isset( $_REQUEST['post_id'] ) ) {
			 $post_id  = absint( $_REQUEST['post_id'] );
			 $post_obj  = get_post( $post_id );
			 $post_slug = sanitize_title($post_obj->post_title).'_';
	 }

   $file['name'] = $site_name.$post_slug.$file_name.$file_ext;
   return $file;

 }


?>
